package darts

import "math"

func Score(x float64, y float64) int {
	lengthFromCenter := math.Sqrt(math.Pow(x, 2) + math.Pow(y, 2))
	switch {
		case lengthFromCenter <= 1.0:
			return 10
		case lengthFromCenter <= 5.0:
			return 5
		case lengthFromCenter <= 10.0:
			return 1
		default:
			return 0
	}
}