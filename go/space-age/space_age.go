package space

import "math"

type Planet string

const EarthYearInSecond = 31557600

var PlanetCoefficients = map[Planet]float64{
	"Earth":   1,
	"Mercury": 0.2408467,
	"Venus":   0.61519726,
	"Mars":    1.8808158,
	"Jupiter": 11.862615,
	"Saturn":  29.447498,
	"Uranus":  84.016846,
	"Neptune": 164.79132,
}

func GetEarthAge(second float64) (year float64) {
	return math.Round(second/EarthYearInSecond*100) / 100
}

func Age(time float64, planet Planet) float64 {
	earth_age := GetEarthAge(time)
	return earth_age / PlanetCoefficients[planet]
}
