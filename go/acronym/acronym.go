package acronym

import "strings"

func ClearString(input string) string {
	return strings.NewReplacer("-", " ", "_", " ").Replace(input)
}

func Abbreviate(phrase string) string {
	clearPhrase := ClearString(phrase)

	phrase_slice := strings.Split(clearPhrase, " ")

	var abbreviate strings.Builder

	for _, word := range phrase_slice {
		if word != "" {
			abbreviate.WriteString(strings.ToUpper(string(word[0])))
		}
	}
	return abbreviate.String()
}
