package collatzconjecture

import "fmt"

func CollatzConjecture(input int) (int, error) {
	if input <= 0 {
		return -1, fmt.Errorf("Invalid input: %d", input)
	}
	iteration := 0
	for input != 1 {
		if input%2 == 0 {
			input /= 2
			iteration++
		} else {
			input = 3*input + 1
			iteration++
		}
	}
	return iteration, nil
}
