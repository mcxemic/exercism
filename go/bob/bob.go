package bob

import (
	"strings"
	"unicode"
)

func HasLetter(s string) bool {
	for _, r := range s {
		if unicode.IsLetter(r) {
			return true
		}
	}
	return false
}

func ClearString(input string) string {
	return strings.Trim(input, "\n\r\t ")
}

func IsQuestion(input string) bool {
	return strings.HasSuffix(input, "?")
}

func IsYell(input string) bool {
	return HasLetter(input) && strings.ToUpper(input) == input
}

func IsEmpty(input string) bool {
	return strings.ToUpper(input) == input
}

func Hey(remark string) string {
	clearString := ClearString(remark)
	switch {
	case IsQuestion(clearString) && IsYell(clearString):
		return "Calm down, I know what I'm doing!"
	case IsYell(clearString):
		return "Whoa, chill out!"
	case IsQuestion(clearString):
		return "Sure."
	case len(clearString) == 0:
		return "Fine. Be that way!"
	default:
		return "Whatever."
	}
}
